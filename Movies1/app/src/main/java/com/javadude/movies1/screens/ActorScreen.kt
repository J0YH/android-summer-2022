package com.javadude.movies1.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies1.Display
import com.javadude.movies1.Label
import com.javadude.movies1.R
import com.javadude.movies1.Screen
import com.javadude.movies1.data.ActorWithRoles
import com.javadude.movies1.data.Movie
import com.javadude.movies1.emptyImmutableList
import com.javadude.movies1.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun ActorScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    actorWithRoles: ActorWithRoles?,
    select: (Movie) -> Unit,
) {
    MovieScaffold(
        scope = scope,
        title = actorWithRoles?.actor?.name ?: stringResource(id = R.string.screen_title_actor),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            Label(label = stringResource(id = R.string.label_movies_starring_actor, actorWithRoles?.actor?.name ?: ""))

            actorWithRoles?.roles
                ?.forEach {
                    Display(text = it.title, modifier = Modifier.clickable { select(it) })
                }
        }
    }
}