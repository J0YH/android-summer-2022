package com.javadude.movies1.screens

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.javadude.movies1.ImmutableList
import com.javadude.movies1.Screen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

// SLOTTED COMPOSABLE FUNCTION

/*

        --------------------------------------
        | Title                      ACTIONS |
        --------------------------------------
        | Content                            |
        |                                    |
        |                                    |
        |                                    |
        |                                    |
        |                                    |
        |                                    |
        --------------------------------------
        | Ratings    | Movies    | Actors    |
        --------------------------------------

Compose Scaffold()
        --------------------------------------
        | TopBar                             |
        --------------------------------------
        | Content                            |
        |                                    |
        |                                    |
        |                                    |
        |                                    |
        |                                    |
        |                                    |
        --------------------------------------
        | BottomBar                          |
        --------------------------------------

 */

@Immutable
data class TopAction(
    val icon: ImageVector,
    @StringRes val contentDescriptionId: Int,
    val onClick: suspend () -> Unit,
)

@Composable
fun MovieScaffold(
    scope: CoroutineScope,
    title: String,
    topActions: ImmutableList<TopAction>,
    currentScreen: Screen,
    screenTargets: ImmutableList<Screen>,
    onScreenSelect: (Screen) -> Unit,
    content: @Composable (Modifier) -> Unit
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = title, modifier = Modifier.padding(8.dp)) },
                actions = {
                    topActions.forEach { action ->
                        IconButton(
                            onClick = {
                                scope.launch(Dispatchers.IO) {
                                    action.onClick()
                                }
                            },
                            modifier = Modifier.padding(8.dp),
                        ) {
                            Icon(
                                imageVector = action.icon,
                                contentDescription = stringResource(id = action.contentDescriptionId)
                            )
                        }
                    }
                }
            )
        },
        content = { paddingValues ->
            content(Modifier.padding(paddingValues))
        },
        bottomBar = {
            BottomNavigation {
                screenTargets.forEach { screen ->
                    val labelText = stringResource(id = screen.titleId)
                    BottomNavigationItem(
                        selected = (screen == currentScreen),
                        icon = {
                            Icon(imageVector = screen.icon, contentDescription = labelText)
                        },
                        label = {
                            Text(text = labelText)
                        },
                        onClick = {
                            onScreenSelect(screen)
                        }
                    )
                }
            }
        }
    )
}