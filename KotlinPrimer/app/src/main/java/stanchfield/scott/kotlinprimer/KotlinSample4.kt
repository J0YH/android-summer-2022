package stanchfield.scott.kotlinprimer

class Button {
    var onClickListener: ((Button) -> Unit)? = null

    // somewhere when user clicks, call onClickListener
    //    at some point onClickListener?.invoke()
}

fun main() {
    val button = Button()
    button.onClickListener = {
        println("Button clicked")
    }
}
