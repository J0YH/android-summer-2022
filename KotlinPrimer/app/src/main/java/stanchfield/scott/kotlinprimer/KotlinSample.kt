package stanchfield.scott.kotlinprimer

class Person1 {
    var age: Int = 0
}

fun mainX() {
    println("Hello, World!")

    var x: Int = 10
    println(x)
    x = 42
    println(x)

    val y = 10
    println(y)
//    y = 15  WON'T WORK!

    val person1 = Person1()
    println(person1.age)
//    person1 = Person1() WON'T WORK
    person1.age = 10

    println(add(5, 4))

    val a = 10

}

private var numberOfPeople: Int = 0

private fun add(a: Int, b: Int): Int {
    println(a); println(b) // DON'T DO THIS!!!
    return a + b
}

private fun add2(a: Int, b: Int): Int = a + b
private fun add3(a: Int, b: Int) = a + b


// java like
private fun add4(a: Int, b: Int) = a + b
private fun add4(a: Int) = a + 10

// simpler in kotlin - default parameters
private fun add5(a: Int, b: Int = 10) = a + b

fun callingAdd5() {
    add5(4, 2)
    add5(4)
}

fun callDoStuff() {
    // using named parameters
    doStuff(
        name = "Scott",
        age = 55,
        x = 10,
        y = 20,  // comma after last parameter is OK
    )
    doStuff(
        10,
        20,
        55,
        "Scott",
    )
}

private fun doStuff(
    x: Int,
    y: Int,
    age: Int,
    name: String,
) {
    println(name + " is " + age + " years old") // BAD BAD BAD BAD
    println("$name is $age years old") // GOOD GOOD GOOD GOOD
    println("X${name}X is $age years old") // GOOD GOOD GOOD GOOD
    println("$name is ${name.length} characters") // GOOD GOOD GOOD GOOD
    println("$name: ${if (age == 10) "yay" else ""}") // GOOD GOOD GOOD GOOD
    // display a point at (x,y)

    var foo: String = ""
    if (age == 10) {
        foo = "hello"
    } else {
        foo = "bye"
    }

    foo = if (age == 10) "hello" else "bye"

    val fee = (if (age == 10) "hello" else "bye")
}

var foo: String? = "hello"

fun nullability() {
    foo = null

//    if (foo != null) {
//        println(foo.length) won't work - something else could have changed it between the if and the use
//    }


    val fooCaptured = foo
    if (fooCaptured != null) {
        println(fooCaptured.length)
    }

    println(foo?.length)
//    println(foo.length) // cannot do!

    printLengthIfNonNull(foo)

    foo?.printLength()

    foo?.doIt(
        // LAMBDA - unnamed function
        {
            println(this.length)
        }
    )
    foo?.doIt {  // LAMBDA - unnamed function
        println(this.length)
    }

    foo?.let { string ->
        println(string.length)
    }
    foo?.let {
        println(it.length)
    }

    doAnother { a, b ->
        a + b
    }
    doAnother { a, b ->
        a - b
    }

    doAnother(::add5)
}

fun printLengthIfNonNull(value: String?) {
    if (value != null) {
        println(value.length)
    }
}

// extension function
fun String.printLength() {
    println(this.length)
}


// HIGHER-ORDER FUNCTIONS
//    takes function(s) as parameters
//    returns a function

fun String.doIt(block: String.() -> Unit) {
    this.block()
}

fun doAnother(block: (Int, Int) -> Int) {
    println(block(5, 10))
}