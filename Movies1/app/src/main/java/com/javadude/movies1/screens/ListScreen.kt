package com.javadude.movies1.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun <T> ListScreen(
    title: String,
    items: List<T>,
    text: T.() -> String,
    select: (T) -> Unit,
) {
    Column {
        Text(text = title, style = MaterialTheme.typography.h4)
        items.forEach {
            Text(text = it.text(), modifier = Modifier.clickable { select(it) })
        }
    }
}

