package com.javadude.movies1.screens

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.javadude.movies1.R
import com.javadude.movies1.Screen
import com.javadude.movies1.data.Rating
import com.javadude.movies1.emptyImmutableList
import com.javadude.movies1.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun RatingsScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    ratings: List<Rating>,
    select: (Rating) -> Unit,
) =
    MovieScaffold(
        scope = scope,
        title = stringResource(id = R.string.screen_title_ratings),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        ListScreen(title = "Ratings", items = ratings, text = { name }, select = select)
    }




