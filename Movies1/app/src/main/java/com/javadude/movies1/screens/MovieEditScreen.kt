package com.javadude.movies1.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import com.javadude.movies1.R
import com.javadude.movies1.Screen
import com.javadude.movies1.TextField
import com.javadude.movies1.data.Movie
import com.javadude.movies1.data.MovieWithRoles
import com.javadude.movies1.emptyImmutableList
import com.javadude.movies1.screenTargets
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun MovieEditScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    movieWithRoles: MovieWithRoles?,
    onMovieChange: suspend (Movie) -> Unit,
) {
    requireNotNull(movieWithRoles)

    MovieScaffold(
        scope = scope,
        title = movieWithRoles.movie.title,
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            TextField(
                labelId = R.string.label_title,
                placeholderId = R.string.placeholder_movie_title,
                value = movieWithRoles.movie.title,
                onValueChange = {
                    scope.launch {
                        onMovieChange(movieWithRoles.movie.copy(title = it))
                    }
                }
            )
            TextField(
                labelId = R.string.label_description,
                placeholderId = R.string.placeholder_movie_description,
                value = movieWithRoles.movie.description,
                onValueChange = {
                    scope.launch {
                        onMovieChange(movieWithRoles.movie.copy(description = it))
                    }
                }
            )
        }
    }
}