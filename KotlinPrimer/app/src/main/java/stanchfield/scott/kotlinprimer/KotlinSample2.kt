package stanchfield.scott.kotlinprimer

class Person2(name: String, age: Int) {
    var name: String = ""
    var age: Int = 0

    init {
        this.name = name
        this.age = age
    }
}

class Person3(var name: String, var age: Int)
data class Person4(var name: String, var age: Int)
    // equals, hashCode, toString, copy, componentN

fun foo2() {
    val person2 = Person2("Scott", 55)
    person2.name = "Mike"
    person2.age = 10
    println(person2.name)
    println(person2.age)
}

fun foo3() {
    val person3 = Person3("Scott", 55)
    person3.name = "Mike"
    person3.age = 10
    println(person3.name)
    println(person3.age)
    println(person3)
}
fun foo4() {
    val person4 = Person4("Scott", 55)
    person4.name = "Mike"
    person4.age = 10
    println(person4.name)
    println(person4.age)
    println(person4)
}

fun mainY() {
    foo3()
    println("----------------------------------------")
    foo4()

    val p1 = Person4("Scott", 55)
    val p2 = Person4("Scott", 55)
    val p3 = Person4("Scott", 10)

    println(p1 == p2)
    println(p2 == p3)

    val p4a = p1.copy(age = 10)
    val p4b = p1.copy(name = "Mike")

    println(p1.component1())
    println(p1.component2())

    // destructuring declaration
    // mostly useful for multiple return values
    val (name, age) = p1

    val (newX, newY) = translate(10, 15)

}


data class NewPoint(val x: Int, val y: Int)
fun translate(x: Int, y: Int) = NewPoint(x+5, y+5)