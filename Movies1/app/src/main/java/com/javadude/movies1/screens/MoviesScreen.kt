package com.javadude.movies1.screens

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.javadude.movies1.R
import com.javadude.movies1.Screen
import com.javadude.movies1.data.Movie
import com.javadude.movies1.emptyImmutableList
import com.javadude.movies1.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun MoviesScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    movies: List<Movie>,
    select: (Movie) -> Unit,
) =
    MovieScaffold(
        scope = scope,
        title = stringResource(id = R.string.screen_title_movies),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        ListScreen(title = "Movies", items = movies, text = { title }, select = select)
    }


