package com.javadude.roomexample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import com.javadude.roomexample.data.Actor
import com.javadude.roomexample.data.ActorWithRoles
import com.javadude.roomexample.data.Movie
import com.javadude.roomexample.data.MovieWithRoles
import com.javadude.roomexample.data.Rating
import com.javadude.roomexample.data.RatingWithMovies
import com.javadude.roomexample.ui.theme.RoomExampleTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val scope = rememberCoroutineScope()

            BackHandler {
                viewModel.pop()
            }

            RoomExampleTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Ui(scope, viewModel) { finish() }
                }
            }
        }
    }
}

@Composable
fun Ui(
    scope: CoroutineScope,
    viewModel: MovieViewModel,
    exit: () -> Unit,
) {
    val ratings by viewModel.ratingsFlow.collectAsState(initial = emptyList())
    val movies by viewModel.moviesFlow.collectAsState(initial = emptyList())
    val actors by viewModel.actorsFlow.collectAsState(initial = emptyList())

    when (viewModel.screen) {
        null -> exit()
        MainScreen -> MainScreen(
            push = viewModel::push,
            onReset = {
                scope.launch {
                    viewModel.resetDatabase()
                }
            }
        )
        ActorsScreen -> ActorsScreen(actors) {
            scope.launch {
//                viewModel.select(it)
                viewModel.push(ActorScreen)
            }
        }
        MoviesScreen -> MoviesScreen(movies) {
            scope.launch {
//                viewModel.select(it)
                viewModel.push(MovieScreen)
            }
        }
        RatingsScreen -> RatingsScreen(ratings) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(RatingScreen)
            }
        }
        ActorScreen -> ActorScreen(viewModel.actor) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(MovieScreen)
            }
        }
        MovieScreen -> MovieScreen(viewModel.movie) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(ActorScreen)
            }
        }
        RatingScreen -> RatingScreen(viewModel.rating) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(MovieScreen)
            }
        }
    }
}

@Composable
fun MainScreen(
    push: (Screen) -> Unit,
    onReset: () -> Unit,
) {
    Column {
        SimpleButton(text = "Ratings") {
            push(RatingsScreen)
        }
        SimpleButton(text = "Movies") {
            push(MoviesScreen)
        }
        SimpleButton(text = "Actors") {
            push(ActorsScreen)
        }
        SimpleButton(text = "Reset DB") {
            onReset()
        }
    }
}

@Composable
fun <T> ListScreen(
    title: String,
    items: List<T>,
    text: T.() -> String,
    select: (T) -> Unit,
) {
    Column {
        Text(text = title, style = MaterialTheme.typography.h4)
        items.forEach {
            Text(text = it.text(), modifier = Modifier.clickable { select(it) })
        }
    }
}

@Composable
fun RatingsScreen(
    ratings: List<Rating>,
    select: (Rating) -> Unit,
) = ListScreen(title = "Ratings", items = ratings, text = { name }, select = select)

@Composable
fun ActorsScreen(
    actors: List<Actor>,
    select: (Actor) -> Unit,
) = ListScreen(title = "Actors", items = actors, text = { name }, select = select)

@Composable
fun MoviesScreen(
    movies: List<Movie>,
    select: (Movie) -> Unit,
) = ListScreen(title = "Movies", items = movies, text = { title }, select = select)

@Composable
fun RatingScreen(
    ratingWithMovies: RatingWithMovies?,
    select: (Movie) -> Unit,
) {
    Column {
        Text(text = ratingWithMovies?.rating?.name ?: "", style = MaterialTheme.typography.h4)
        ratingWithMovies?.movies?.forEach {
            Text(text = it.title, modifier = Modifier.clickable { select(it) })
        }
    }
}

@Composable
fun MovieScreen(
    movieWithRoles: MovieWithRoles?,
    select: (Actor) -> Unit,
) {
    Column {
        Text(text = movieWithRoles?.movie?.title ?: "", style = MaterialTheme.typography.h4)
        movieWithRoles?.roles
//            ?.sortedBy { it.orderInCredits }
            ?.forEach {
//            Text(text = """${it.character}: ${it.actor.name}""", modifier = Modifier.clickable { select(it.actor) })
            Text(text = it.name, modifier = Modifier.clickable { select(it) })
        }
    }
}

@Composable
fun ActorScreen(
    actorWithRoles: ActorWithRoles?,
    select: (Movie) -> Unit,
) {
    Column {
        Text(text = actorWithRoles?.actor?.name ?: "", style = MaterialTheme.typography.h4)
        actorWithRoles?.roles
//            ?.sortedBy { it.orderInCredits }
            ?.forEach {
//            Text(text = """${it.character}: ${it.actor.name}""", modifier = Modifier.clickable { select(it.movie) })
            Text(text = it.title, modifier = Modifier.clickable { select(it) })
        }
    }
}
