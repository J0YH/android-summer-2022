package com.javadude.roomexample

import com.javadude.roomexample.data.Actor
import com.javadude.roomexample.data.ActorWithRoles
import com.javadude.roomexample.data.Movie
import com.javadude.roomexample.data.MovieWithRoles
import com.javadude.roomexample.data.Rating
import com.javadude.roomexample.data.RatingWithMovies
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val ratingsFlow: Flow<List<Rating>>
    val moviesFlow: Flow<List<Movie>>
    val actorsFlow: Flow<List<Actor>>

    suspend fun expand(rating: Rating): RatingWithMovies
    suspend fun expand(movie: Movie): MovieWithRoles
    suspend fun expand(actor: Actor): ActorWithRoles

    suspend fun resetDatabase()
}