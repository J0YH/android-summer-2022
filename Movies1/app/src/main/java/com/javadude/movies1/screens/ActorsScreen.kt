package com.javadude.movies1.screens

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.javadude.movies1.R
import com.javadude.movies1.Screen
import com.javadude.movies1.data.Actor
import com.javadude.movies1.emptyImmutableList
import com.javadude.movies1.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun ActorsScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    actors: List<Actor>,
    select: (Actor) -> Unit,
) =
    MovieScaffold(
        scope = scope,
        title = stringResource(id = R.string.screen_title_actors),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        ListScreen(title = "Actors", items = actors, text = { name }, select = select)
    }

